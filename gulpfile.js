'use strict';

var gulp         = require('gulp'),
    concat       = require('gulp-concat'),
    sass         = require('gulp-sass'),
    rename       = require('gulp-rename'),
    autoprefixer = require('gulp-autoprefixer'),
    cleancss     = require('gulp-clean-css'),
    uglify       = require('gulp-uglify'),
    del          = require('del'),
    browserSync  = require('browser-sync').create(),
    fileinclude  = require('gulp-file-include');
    // cssnano      = require('gulp-cssnano'),
    // imagemin     = require('gulp-imagemin'),
    // pngquant     = require('imagemin-pngquant'),
    // notify       = require("gulp-notify"),


/*  ----==== All Gulps Moduls ====----

    1. Scss -> Css
    2. Js Combining
    3. Delete Dist Folder
    4. Html Component Include
    5. Watch and BrowserSync
    6. Build project
    7. Watching Scss

*/


// 1. Scss => Css
function scss() {
    return gulp.src('app/scss/**/*.scss')
        .pipe(sass({ outputStyle: 'expanded' }).on("error", sass.logError))
        .pipe(rename({ suffix: '.min', prefix : '' }))
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
        // .pipe(cleancss( {level: { 1: { specialComments: 0 } } }))
        .pipe(gulp.dest('app/css'))
        .pipe(browserSync.stream());
}

// 2. Libs scripts concat
function scripts() {
    return gulp.src([
        'app/libs/jquery/dist/jquery.min.js',
        'app/libs/bootstrap/dist/js/bootstrap.min.js'
        // 'app/libs/Waves/dist/waves.min.js'
    ])
        .pipe(concat('libs.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('app/js'));
}

// 3. Build directory clean
function clean() {
    return del('build');
}

// 4. Html Component Include
function fileincludeone() {
    gulp.src(['app/html/*.html'])
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(gulp.dest('app/'));
}

// 5. Watch and BrowserSync
function watch() {
    browserSync.init({
        server: {
            baseDir: "app"
        }
    });

    gulp.watch('app/scss/**/*.scss' , scss);
    gulp.watch('app/js/**/*.js').on('change' ,browserSync.reload);
    gulp.watch('app/html/**/*.html').on('change', gulp.parallel(fileincludeone , browserSync.reload));
}

// 6. Build project
function build() {
    var buildCss = gulp.src([
        'app/css/app.min.css'
    ])
        .pipe(gulp.dest('build/css'));

    var buildFonts = gulp.src('app/fonts/**/*')
        .pipe(gulp.dest('build/fonts'));

    var buildJs = gulp.src('app/js/**/*')
        .pipe(gulp.dest('build/js'));

    var buildHtml = gulp.src('app/*.html')
        .pipe(gulp.dest('build'));

    var buildImages = gulp.src(['app/img/**/*.{gif,jpg,png,svg}'])
          .pipe(gulp.dest('build/img'));
}


// Call Functions

// 1. Scss
gulp.task('scss' , scss);

// 2. Scripts
gulp.task('scripts' , scripts);

// 3. Clean
gulp.task('clean' , clean);

// 4. Include
gulp.task('fileinclude' , fileincludeone);

// 5. Watch
gulp.task('watch' , gulp.parallel(watch , fileincludeone , scss , scripts));

// 5. Build
gulp.task('build', gulp.series(clean ,gulp.parallel(scss , scripts) , build));

// 6. Default
gulp.task('default' , gulp.series('watch'));